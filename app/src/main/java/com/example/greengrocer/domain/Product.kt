package com.example.greengrocer.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

object Product {

    private val products = MutableLiveData<MutableList<ProductModel>>()

    val productItems: LiveData<MutableList<ProductModel>>
        get() = products

    init {
        fillProducts()
    }

    private fun fillProducts() {
        val list: MutableList<ProductModel> = mutableListOf()

        list.add(ProductModel(1, "Watermelon", 10, ProductType.FRUIT, "Watermelon"))
        list.add(ProductModel(2, "GrapeFruit", 5, ProductType.FRUIT, "Grapefruit"))
        list.add(ProductModel(3, "Kiwi", 15, ProductType.FRUIT, "kiwi"))
        list.add(ProductModel(4, "Cucumber", 8, ProductType.VEGETABLE, "Cucumber"))
        list.add(ProductModel(5, "Avocado", 6, ProductType.VEGETABLE, "Avocado"))

        products.value = list
    }

    fun getProduct(id: Int): ProductModel? {
        val list: MutableList<ProductModel> = products.value ?: mutableListOf()

        return list.find { it.id == id }
    }

}