package com.example.greengrocer.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

object Cart {

    private val cartItems = MutableLiveData<MutableList<CartModel>>()

    val items: LiveData<MutableList<CartModel>>
        get() = cartItems

    // var cartItems: MutableList<CartModel> = mutableListOf()
    // var totalPrice = MutableLiveData<Number>(0)


    fun addProduct (id: Int) {

            val list: MutableList<CartModel> = cartItems.value ?: mutableListOf()

            val indexItem = list.indexOfFirst { it.productId == id }.takeIf { it !== null } ?: -1
            if (indexItem != -1) {
                list[indexItem].quantity = list[indexItem].quantity + 1
                cartItems.value = list
            } else {
                list.add(CartModel(id, 1))
                cartItems.value = list
            }
    }

    fun removeProduct (id: Int) {

        val list: MutableList<CartModel> = cartItems.value ?: mutableListOf()

        val indexItem = list.indexOfFirst { it.productId == id }.takeIf { it !== null } ?: -1
        if (indexItem != -1 && list[indexItem]?.quantity > 1) {
            list[indexItem].quantity = list[indexItem].quantity -1
            cartItems.value = list
        }
        if (indexItem != -1 && list[indexItem]?.quantity == 1) {
            list.removeAt(indexItem)
            cartItems.value = list
        }
    }

    fun setQuantityProducts (id: Int, quantity: Int) {
        val list: MutableList<CartModel> = cartItems.value ?: mutableListOf()

        if(quantity > 0) {
            val itemsCart = list.map {
                if (it.productId == id) {
                    it.quantity = quantity
                }
                it
            }
            cartItems.value = itemsCart.toMutableList()
        } else {
            cartItems.value = list.filter { it.productId != id }.toMutableList()
        }
    }

    fun emptyCart () {
        cartItems.value = null
    }

    fun getQuantity(id: Int): Int {
        val list: MutableList<CartModel> = cartItems.value ?: mutableListOf()

        val indexItem = list.indexOfFirst { it.productId == id }.takeIf { it !== null } ?: -1

        return list[indexItem].quantity ?: 0
    }

}