package com.example.greengrocer.domain

enum class ProductType { FRUIT, VEGETABLE }

data class ProductModel (val id: Int,
                    val name: String,
                    val price: Int,
                    val type: ProductType,
                    val image: String)