package com.example.greengrocer.domain

data class ItemProduct (val id: Int,
                   val name: String,
                   val price: Int,
                   val type: ProductType,
                   var quantity: Int?)