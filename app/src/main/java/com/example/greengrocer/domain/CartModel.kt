package com.example.greengrocer.domain

data class CartModel (val productId: Int, var quantity: Int)