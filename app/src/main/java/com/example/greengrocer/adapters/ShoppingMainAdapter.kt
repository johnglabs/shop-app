package com.example.greengrocer.adapters

import android.annotation.SuppressLint
import android.content.ClipData
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.greengrocer.R
import com.example.greengrocer.databinding.ProductItemBinding
import com.example.greengrocer.domain.ItemProduct
import com.example.greengrocer.domain.ProductModel
import com.example.greengrocer.domain.ProductType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

private val ITEM_VIEW_TYPE_HEADER_F = 0
private val ITEM_VIEW_TYPE_HEADER_V = 1
private val ITEM_VIEW_TYPE_ITEM = 2

class ShoppingMainAdapter(val clickListener: ShoppingMainListener): ListAdapter<DataItem,
        RecyclerView.ViewHolder>(ProductDiffCallback()) {

    private val adapterScope = CoroutineScope(Dispatchers.Default)

    fun addHeaderAndSubmitList(list: List<ItemProduct>?) {
        adapterScope.launch {
            val items = when (list) {
                null -> listOf(DataItem.HeaderF)
                else -> buildList(list)
            }
            withContext(Dispatchers.Main) {
                submitList(items)
            }
        }
    }

    private fun buildList(list: List<ItemProduct>): List<DataItem> {
        val fruits = list.filter {
            it.type == ProductType.FRUIT
        }
        val veggies = list.filter {
            it.type == ProductType.VEGETABLE
        }
        val fruitHeader = if (fruits?.size > 0) listOf(DataItem.HeaderF) else listOf()
        val vegetableHeader = if (veggies?.size > 0) listOf(DataItem.HeaderV) else listOf()

        return fruitHeader + fruits.map { DataItem.ProductItem(it) } + vegetableHeader + veggies.map { DataItem.ProductItem(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_VIEW_TYPE_HEADER_F -> TextViewHolder.from(parent, ITEM_VIEW_TYPE_HEADER_F)
            ITEM_VIEW_TYPE_HEADER_V -> TextViewHolder.from(parent, ITEM_VIEW_TYPE_HEADER_V)
            ITEM_VIEW_TYPE_ITEM -> ShoppingViewHolder.from(parent)
            else -> throw ClassCastException("Unknown viewType ${viewType}")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ShoppingViewHolder -> {
                val productItem = getItem(position) as DataItem.ProductItem
                holder.bind(clickListener, productItem.product)
            }
        }
    }

    class TextViewHolder(view: View): RecyclerView.ViewHolder(view) {
        companion object {
            fun from(parent: ViewGroup, type: Int): TextViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)

                val view = when(type) {
                    ITEM_VIEW_TYPE_HEADER_F -> layoutInflater.inflate(R.layout.header_fruit, parent, false)
                    ITEM_VIEW_TYPE_HEADER_V -> layoutInflater.inflate(R.layout.header_veggie, parent, false)
                    else -> throw ClassCastException("Unknown viewType")
                }
                return TextViewHolder(view)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is DataItem.HeaderF -> ITEM_VIEW_TYPE_HEADER_F
            is DataItem.HeaderV -> ITEM_VIEW_TYPE_HEADER_V
            is DataItem.ProductItem -> ITEM_VIEW_TYPE_ITEM
        }
    }

    class ShoppingViewHolder private constructor(val viewDataBinding: ProductItemBinding) :
        RecyclerView.ViewHolder(viewDataBinding.root) {

        fun bind(clickListener: ShoppingMainListener, item: ItemProduct) {
            val res = itemView.context.resources
            // viewDataBinding.nameProduct.text = item.name
            viewDataBinding.product = item
            viewDataBinding.clickListener = clickListener
            viewDataBinding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ShoppingViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ProductItemBinding.inflate(layoutInflater, parent, false)
                return ShoppingViewHolder(binding)
            }
        }
    }
}

class ProductDiffCallback : DiffUtil.ItemCallback<DataItem>() {
    override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem.id == newItem.id
    }
    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem == newItem
    }
}

class ShoppingMainListener(val clickListener: (productId: Int, option: Int) -> Unit) {
    fun onClick(product: ItemProduct, option: Int) = clickListener(product.id, option)
}

sealed class DataItem {
    data class ProductItem(val product: ItemProduct): DataItem() {
        override val id = product.id
    }

    object HeaderF: DataItem() {
        override val id = Int.MIN_VALUE
    }

    object HeaderV: DataItem() {
        override val id = Int.MIN_VALUE
    }

    abstract val id: Int
}