package com.example.greengrocer.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.example.greengrocer.R
import com.example.greengrocer.domain.ItemBanner

class BannerAdapter: RecyclerView.Adapter<BannerAdapter.ViewHolder>() {

    var data = listOf<ItemBanner>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BannerAdapter.ViewHolder {
       return ViewHolder.from(parent, viewType)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: BannerAdapter.ViewHolder, position: Int) {
        val item = data[position]
        holder.nameBanner.text = item.name
        holder.imageBanner.setImageResource(item.image)
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val imageBanner: ImageView = itemView.findViewById(R.id.image_banner)
        val nameBanner: TextView = itemView.findViewById(R.id.name_banner)

        companion object {
            fun from(parent: ViewGroup, type: Int): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)

                val view = layoutInflater.inflate(R.layout.item_banner, parent, false)
                return ViewHolder(view)
            }
        }
    }
}