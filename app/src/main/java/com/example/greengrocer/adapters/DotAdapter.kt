package com.example.greengrocer.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.*
import androidx.recyclerview.widget.RecyclerView
import com.example.greengrocer.R
import com.example.greengrocer.domain.ItemBanner

class DotAdapter: RecyclerView.Adapter<DotAdapter.ViewHolder>() {

    var activePosition = 0
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DotAdapter.ViewHolder {
        return ViewHolder.from(parent, viewType)
    }

    override fun getItemCount() = 4

    override fun onBindViewHolder(holder: DotAdapter.ViewHolder, position: Int) {
        if(activePosition == position) {
            holder.dotView.setBackgroundColor(Color.parseColor("#212121"))
        } else {
            holder.dotView.setBackgroundColor(Color.parseColor("#bdbdbd"))
        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val dotView: View = itemView.findViewById(R.id.dot_view)

        companion object {
            fun from(parent: ViewGroup, type: Int): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)

                val view = layoutInflater.inflate(R.layout.item_dot, parent, false)
                return ViewHolder(view)
            }
        }
    }
}