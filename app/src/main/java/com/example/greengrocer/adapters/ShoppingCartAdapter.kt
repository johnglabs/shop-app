package com.example.greengrocer.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.greengrocer.R
import com.example.greengrocer.databinding.CartViewItemBinding
import com.example.greengrocer.databinding.ProductItemBinding
import com.example.greengrocer.domain.CartModel
import com.example.greengrocer.domain.ItemProduct
import com.example.greengrocer.domain.ProductModel

class ShoppingCartAdapter(val clickListener: ShoppingCartListener): ListAdapter<CartModel,
        ShoppingCartAdapter.ShoppingViewHolder>(CartItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoppingViewHolder {
        return ShoppingViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ShoppingViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(clickListener, item)
    }

    class ShoppingViewHolder private constructor(val viewDataBinding: CartViewItemBinding) :
        RecyclerView.ViewHolder(viewDataBinding.root) {

        fun bind(clickListener: ShoppingCartListener, item: CartModel) {
            viewDataBinding.cartItem = item
            viewDataBinding.clickListener = clickListener
            viewDataBinding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ShoppingViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = CartViewItemBinding.inflate(layoutInflater, parent, false)
                return ShoppingViewHolder(binding)
            }
        }
    }
}

class CartItemDiffCallback : DiffUtil.ItemCallback<CartModel>() {
    override fun areItemsTheSame(oldItem: CartModel, newItem: CartModel): Boolean {
        return oldItem.productId == newItem.productId
    }
    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: CartModel, newItem: CartModel): Boolean {
        return oldItem == newItem
    }
}

class ShoppingCartListener(val clickListener: (product: CartModel) -> Unit) {
    fun onClick(product: CartModel) = clickListener(product)
}