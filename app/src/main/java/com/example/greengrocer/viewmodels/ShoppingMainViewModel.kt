package com.example.greengrocer.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.greengrocer.R
import com.example.greengrocer.domain.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ShoppingMainViewModel: ViewModel() {

    private val _itemActiveBanner = MutableLiveData<Int>()

    val itemActiveBanner: LiveData<Int>
        get() = _itemActiveBanner

    val products = Product.productItems

    val cartItems = Cart.items

    val bannerImages: List<ItemBanner> = listOf(
        ItemBanner(R.drawable.banner_1, "Brazilian Banana"),
        ItemBanner(R.drawable.banner_2, "Orange"),
        ItemBanner(R.drawable.banner_3, "Cucumber"),
        ItemBanner(R.drawable.banner_4, "Kiwi")
    )

    fun setItemActiveBanner(position: Int) {
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                _itemActiveBanner.value = position
            }
        }

    }

    fun onProductClicked(id: Int, option: Int) {
        if (option == 1) {
            Cart.addProduct(id)
        } else if (option == 0) {
            Cart.removeProduct(id)
        }
    }
}