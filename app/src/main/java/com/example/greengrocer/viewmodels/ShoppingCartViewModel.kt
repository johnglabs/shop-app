package com.example.greengrocer.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.greengrocer.domain.*

class ShoppingCartViewModel: ViewModel() {

    private val _itemDialog = MutableLiveData<CartModel>()

    val itemDialog: LiveData<CartModel>
        get() = _itemDialog

    val products = Product.productItems

    val cartItems = Cart.items

    val totalPrice = Transformations.map(cartItems) {items ->
        val sum = items?.fold(0) { sum, cartModel ->
            val product = Product.getProduct(cartModel.productId)
            return@fold cartModel.quantity * (product?.price ?: 1) + sum
        } ?: 0
        return@map "$$sum"
    }

    fun emptyCart() {
        Cart.emptyCart()
    }

    fun setItemCart(item: CartModel?) {
        _itemDialog.value = item
    }

    fun setQuantityItemCart(id: Int, quantity: Int) {
        Cart.setQuantityProducts(id, quantity)
    }
}