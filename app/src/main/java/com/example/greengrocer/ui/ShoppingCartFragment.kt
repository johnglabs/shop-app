package com.example.greengrocer.ui

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.greengrocer.R
import com.example.greengrocer.adapters.ShoppingCartAdapter
import com.example.greengrocer.adapters.ShoppingCartListener
import com.example.greengrocer.databinding.FragmentShoppingCartBinding
import com.example.greengrocer.databinding.FragmentShoppingMainBinding
import com.example.greengrocer.domain.CartModel
import com.example.greengrocer.viewmodels.ShoppingCartViewModel

class ShoppingCartFragment: Fragment() {

    private var itemPopUp: Dialog? = null
    private lateinit var viewModel: ShoppingCartViewModel
    private lateinit var adapter: ShoppingCartAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentShoppingCartBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_shopping_cart, container, false)

        viewModel = ViewModelProvider(this).get(ShoppingCartViewModel::class.java)

        binding.lifecycleOwner = this

        binding.shoppingCartViewModel = viewModel

        adapter = ShoppingCartAdapter(ShoppingCartListener { product ->
            viewModel.setItemCart(product)
        })

        binding.listCart.adapter = adapter

        viewModel.cartItems.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.submitList(it)
            }
        })

        viewModel.itemDialog.observe(viewLifecycleOwner, Observer {
            it?.let {
                itemPopUp = showItemPopUp(it)
            } ?: run {
                itemPopUp?.dismiss()
            }
        })

        binding.checkoutButton.setOnClickListener {
            val builder = AlertDialog.Builder(context)
            builder.setTitle("Purchase Successful")
            builder.setMessage("Your purchase was done")
            builder.setPositiveButton("OK", DialogInterface.OnClickListener { _, _ ->
                viewModel.emptyCart()
                this.findNavController().popBackStack()
            })
            builder.show()
        }

        binding.toolbarIconBack.setOnClickListener {
            this.findNavController().popBackStack()
        }

        return binding.root
    }


    private fun showItemPopUp(itemCart: CartModel): Dialog {
        val layoutInflater = LayoutInflater.from(context)
        val itemCartPopUp = layoutInflater.inflate(R.layout.dialog_item_cart, null)

        var quantity = itemCart.quantity
        val textItem = itemCartPopUp.findViewById<TextView>(R.id.quantity_item)
        textItem.text = quantity.toString()

        val buttonPlus = itemCartPopUp.findViewById<Button>(R.id.plusButton)
        buttonPlus.setOnClickListener {
            quantity++
            textItem.text = quantity.toString()
        }

        val buttonRemove = itemCartPopUp.findViewById<Button>(R.id.removeBtn)
        buttonRemove.setOnClickListener {
            if (quantity > 0) {
                quantity--
                textItem.text = quantity.toString()
            }
        }

        val buttonCancel = itemCartPopUp.findViewById<Button>(R.id.cancel)
        buttonCancel.setOnClickListener {
            viewModel.setItemCart(null)
        }

        val buttonSave = itemCartPopUp.findViewById<Button>(R.id.save)
        buttonSave.setOnClickListener {
            viewModel.setQuantityItemCart(itemCart.productId, quantity)
            viewModel.setItemCart(null)
            adapter.notifyDataSetChanged()
        }

        val messageDialog = Dialog(requireContext())
        messageDialog.setContentView(itemCartPopUp)
        messageDialog.show()
        return messageDialog
    }

}