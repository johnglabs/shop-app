package com.example.greengrocer.ui

import android.os.Bundle
import android.view.*
import android.widget.ImageView
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.greengrocer.R
import com.example.greengrocer.adapters.BannerAdapter
import com.example.greengrocer.adapters.DotAdapter
import com.example.greengrocer.adapters.ShoppingMainAdapter
import com.example.greengrocer.adapters.ShoppingMainListener
import com.example.greengrocer.databinding.FragmentShoppingMainBinding
import com.example.greengrocer.domain.CartModel
import com.example.greengrocer.domain.ItemProduct
import com.example.greengrocer.domain.ProductModel
import com.example.greengrocer.viewmodels.ShoppingMainViewModel
import com.synnapps.carouselview.CarouselView
import com.synnapps.carouselview.ImageListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class ShoppingMainFragment: Fragment() {

    private lateinit var viewModel: ShoppingMainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentShoppingMainBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_shopping_main, container, false)

        viewModel = ViewModelProvider(this).get(ShoppingMainViewModel::class.java)

        binding.lifecycleOwner = this

        binding.shoppingMainViewModel = viewModel

        val adapter = ShoppingMainAdapter(ShoppingMainListener { productId, option ->
            viewModel.onProductClicked(productId, option)
        })

        val bannerAdapter = BannerAdapter()
        val dotsAdapter = DotAdapter()

        binding.productsList.layoutManager = LinearLayoutManager(activity)
        binding.productsList.adapter = adapter

        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.bannerView.layoutManager = layoutManager
        binding.bannerView.adapter = bannerAdapter

        val layoutManager2 = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.dotsView.adapter = dotsAdapter
        binding.dotsView.layoutManager = layoutManager2

        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(binding.bannerView)

        bannerAdapter.data = viewModel.bannerImages
        // dotsAdapter.data = viewModel.bannerImages
        val timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                if(layoutManager.findLastCompletelyVisibleItemPosition() < (bannerAdapter.itemCount - 1)) {
                    viewModel.setItemActiveBanner(layoutManager.findLastCompletelyVisibleItemPosition() + 1)
                    layoutManager.smoothScrollToPosition(binding.bannerView, RecyclerView.State(), layoutManager.findLastCompletelyVisibleItemPosition() + 1)
                } else if (layoutManager.findLastCompletelyVisibleItemPosition() == (bannerAdapter.itemCount - 1)) {
                    viewModel.setItemActiveBanner(0)
                    layoutManager.smoothScrollToPosition(binding.bannerView, RecyclerView.State(), 0)
                }
            }
        }, 0, 3000)

        binding.search.addTextChangedListener {
            val list = searchElements(it.toString(), viewModel.products.value)
            val finalList = combineLists(viewModel.cartItems.value, list)
            adapter.addHeaderAndSubmitList(finalList)
        }

        viewModel.itemActiveBanner.observe(viewLifecycleOwner, Observer {
            it?.let {
                dotsAdapter.activePosition = it
            }
        })

        viewModel.products.observe(viewLifecycleOwner, Observer {
            it?.let {
                val list = combineLists(viewModel.cartItems.value, it)
                adapter.addHeaderAndSubmitList(list)
            }
        })

        viewModel.cartItems.observe(viewLifecycleOwner, Observer {
            it?.let {
                val list = combineLists(it, viewModel.products.value)
                adapter.addHeaderAndSubmitList(list)
            }
        })

        binding.toolbarIconCart.setOnClickListener {
            onNavigate()
        }

        return binding.root
    }

    private suspend fun update(position: Int) {
        withContext(Dispatchers.Default) {
            viewModel.setItemActiveBanner(0)
        }
    }

    private fun combineLists(
        cartItems: MutableList<CartModel>?,
        products: MutableList<ProductModel>?
    ): List<ItemProduct>? {
        return products?.map {
            val newItem = ItemProduct(it.id, it.name, it.price, it.type, 0)
            val indexItem =
                cartItems?.indexOfFirst { item -> item.productId == it.id }.takeIf { it !== null } ?: -1
            if (cartItems != null && indexItem != -1) {
                newItem.quantity = cartItems[indexItem].quantity
            }
            newItem
        }
    }

    private fun searchElements(text: String, list: MutableList<ProductModel>?): MutableList<ProductModel>? {
        return list?.filter { it.name.contains(text, ignoreCase = true) } as MutableList<ProductModel>
    }

    private fun onNavigate() {
        this.findNavController().navigate(R.id.action_shoppingMainFragment_to_shoppingCartFragment)
    }
}