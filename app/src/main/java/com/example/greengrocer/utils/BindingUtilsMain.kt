package com.example.greengrocer.utils

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.greengrocer.R
import com.example.greengrocer.domain.*

@BindingAdapter("productImage")
fun ImageView.setProductImage(item: ItemProduct?) {
    item?.let {
        setImageResource(when (it.id) {
            1 -> R.drawable.watermelon
            2 -> R.drawable.grapefruit
            3 -> R.drawable.kiwi
            4 -> R.drawable.cucumber
            5 -> R.drawable.avocado
            else -> R.drawable.watermelon
        })
    }
}


@BindingAdapter("productName")
fun TextView.setProductName(item: ItemProduct?) {
    item?.let {
        text = item.name
    }
}

@BindingAdapter("productPrice")
fun TextView.setProductPrice(item: ItemProduct?) {
    item?.let {
        text = "$ "+item.price
    }
}

@BindingAdapter("productQuantity")
fun TextView.setProductQuantity(item: ItemProduct?) {
    item?.let {
        text = (item?.quantity ?: 0).toString()
    }
}

@BindingAdapter("visibleButton")
fun Button.setVisibleButton(item: ItemProduct?) {
    item?.let {
        visibility = if (item.quantity !== 0) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }
}

@BindingAdapter("visibleBox")
fun LinearLayout.setVisibleBox(item: ItemProduct?) {
    item?.let {
        visibility = if (item.quantity === 0) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }
}

@BindingAdapter("cardItemQuantity")
fun TextView.setCardItemQuantity(item: CartModel?) {
    item?.let {
        text = item.quantity.toString() + " Unit"
    }
}

@BindingAdapter("cardItemImage")
fun ImageView.setCardItemImage(item: CartModel?) {
    item?.let {
        setImageResource(when (it.productId) {
            1 -> R.drawable.watermelon
            2 -> R.drawable.grapefruit
            3 -> R.drawable.kiwi
            4 -> R.drawable.cucumber
            5 -> R.drawable.avocado
            else -> R.drawable.watermelon
        })
    }
}

@BindingAdapter("cardItemName")
fun TextView.setCardItemName(item: CartModel?) {
    item?.let {
        val product = Product.getProduct(it.productId)
        text = product?.name
    }
}

@BindingAdapter("cardItemPrice")
fun TextView.setCardItemPrice(item: CartModel?) {
    item?.let {
        val product = Product.getProduct(it.productId)
        text = "$ "+ product?.price
    }
}
